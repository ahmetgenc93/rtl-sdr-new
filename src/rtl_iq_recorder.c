/*
 * rtl-sdr, turns your Realtek RTL2832 based DVB dongle into a SDR receiver
 * Copyright (C) 2012 by Steve Markgraf <steve@steve-m.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>
#include <io.h>
#include <fcntl.h>
#include "getopt/getopt.h"
#endif

#include "rtl-sdr.h"
#include "convenience/convenience.h"

#define DEFAULT_SAMPLE_RATE		2048000
#define DEFAULT_BUF_LENGTH		(16 * 16384)
#define MINIMAL_BUF_LENGTH		512
#define MAXIMAL_BUF_LENGTH		(256 * 16384)

#define ONE_HOUR_TO_MIN 60
#define ONE_MIN_TO_SEC 60
#define ONE_SEC_TO_USEC 1000.0
#define ONE_UHOUR ONE_HOUR_TO_MIN*ONE_MIN_TO_SEC*ONE_SEC_TO_USEC

#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)

static int do_exit = 0;
static uint32_t bytes_to_read = 0;
static rtlsdr_dev_t *dev = NULL;

static int record_sec = 60;

void usage(void)
{
	fprintf(stderr,
		"rtl_sdr, an I/Q recorder for RTL2832 based DVB-T receivers\n\n"
		"Usage:\t -f frequency_to_tune_to [Hz]\n"
		"\t[-s samplerate (default: 2048000 Hz)]\n"
		"\t[-d device_index (default: 0)]\n"
		"\t[-g gain (default: 0 for auto)]\n"
		"\t[-p ppm_error (default: 0)]\n"
		"\t[-b output_block_size (default: 16 * 16384)]\n"
		"\t[-n number of samples to read (default: 0, infinite)]\n"
		"\t[-S force sync output (default: async)]\n"
		"\t[-t time (sec) for recording (default: 60sec)]\n"
		"\tfilename (a '-' dumps samples to stdout)\n\n");
	exit(1);
}

static void sighandler(int signum)
{
	fprintf(stderr, "Signal caught, exiting!\n");
	do_exit = 1;
	rtlsdr_cancel_async(dev);
}

static void rtlsdr_callback(unsigned char *buf, uint32_t len, void *ctx)
{
	if (ctx) {
		if (do_exit)
			return;

		if ((bytes_to_read > 0) && (bytes_to_read < len)) {
			len = bytes_to_read;
			do_exit = 1;
			rtlsdr_cancel_async(dev);
		}

		if (fwrite(buf, 1, len, (FILE*)ctx) != len) {
			fprintf(stderr, "Short write, samples lost, exiting!\n");
			rtlsdr_cancel_async(dev);
		}

		if (bytes_to_read > 0)
			bytes_to_read -= len;
	}
}

char * generate_iq_file_name(const char* filename) {

	struct timeval timestamp;
    time_t current_time;

    current_time = time(NULL);
    struct tm *tm = localtime(&current_time);
	gettimeofday(&timestamp, NULL);
	char *new_file_name = (char *) malloc(100 * sizeof(char));
    memset(new_file_name, 0, sizeof(new_file_name));
    sprintf(new_file_name, "%s_%02d-%02d-%d_%02d:%02d:%02d_%03ld.raw", filename, tm->tm_mday, tm->tm_mon+1, tm->tm_year+1900, tm->tm_hour, tm->tm_min, tm->tm_sec, timestamp.tv_usec%1000);
	return new_file_name;
}

FILE * create_iq_file(FILE* fptr, const char* filename) {
    char full_path[1280];

    if(fptr == NULL){ // first time to open the file
        fptr = fopen(filename, "w");
        if(fptr == NULL)
            return NULL;
            
    }
    fprintf(stdout, "new raw file created with name %s\n", filename);
    return fptr;
}

int main(int argc, char **argv)
{
	struct sigaction sigact;
	char *filename = NULL;
	int n_read;
	int r, opt;
	int gain = 0;
	int ppm_error = 0;
	int sync_mode = 0;
	FILE *file;
	uint8_t *buffer;
	int dev_index = 0;
	int dev_given = 0;
	uint32_t frequency = 100000000;
	uint32_t samp_rate = DEFAULT_SAMPLE_RATE;
	uint32_t out_block_size = DEFAULT_BUF_LENGTH;

	while ((opt = getopt(argc, argv, "d:f:g:s:b:n:t:p:S")) != -1) {
		switch (opt) {
		case 'd':
			dev_index = verbose_device_search(optarg);
			dev_given = 1;
			break;
		case 'f':
			frequency = (uint32_t)atofs(optarg);
			break;
		case 'g':
			gain = (int)(atof(optarg) * 10); /* tenths of a dB */
			break;
		case 's':
			samp_rate = (uint32_t)atofs(optarg);
			break;
		case 'p':
			ppm_error = atoi(optarg);
			break;
		case 'b':
			out_block_size = (uint32_t)atof(optarg);
			break;
		case 'n':
			bytes_to_read = (uint32_t)atof(optarg) * 2;
			break;
		case 'S':
			sync_mode = 1;
			break;
		case 't':
			record_sec = (int)atoi(optarg);
			break;
		default:
			usage();
			break;
		}
	}

	if (argc <= optind) {
		usage();
	} else {
		filename = argv[optind];
	}

	if(out_block_size < MINIMAL_BUF_LENGTH ||
	   out_block_size > MAXIMAL_BUF_LENGTH ){
		fprintf(stderr,
			"Output block size wrong value, falling back to default\n");
		fprintf(stderr,
			"Minimal length: %u\n", MINIMAL_BUF_LENGTH);
		fprintf(stderr,
			"Maximal length: %u\n", MAXIMAL_BUF_LENGTH);
		out_block_size = DEFAULT_BUF_LENGTH;
	}

	buffer = malloc(out_block_size * sizeof(uint8_t));

	if (!dev_given) {
		dev_index = verbose_device_search("0");
	}

	if (dev_index < 0) {
		exit(1);
	}

	r = rtlsdr_open(&dev, (uint32_t)dev_index);
	if (r < 0) {
		fprintf(stderr, "Failed to open rtlsdr device #%d.\n", dev_index);
		exit(1);
	}
	sigact.sa_handler = sighandler;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigaction(SIGINT, &sigact, NULL);
	sigaction(SIGTERM, &sigact, NULL);
	sigaction(SIGQUIT, &sigact, NULL);
	sigaction(SIGPIPE, &sigact, NULL);
	/* Set the sample rate */
	verbose_set_sample_rate(dev, samp_rate);

	/* Set the frequency */
	verbose_set_frequency(dev, frequency);

	if (0 == gain) {
		 /* Enable automatic gain */
		verbose_auto_gain(dev);
	} else {
		/* Enable manual gain */
		gain = nearest_gain(dev, gain);
		verbose_gain_set(dev, gain);
	}

	verbose_ppm_set(dev, ppm_error);
	
	if(strcmp(filename, "-") == 0) { /* Write samples to stdout */
		file = stdout;
	} else {
		const char* file_name = generate_iq_file_name(filename);
		file = fopen(file_name, "wb");
		if (!file) {
			fprintf(stderr, "Failed to open %s\n", filename);
			goto out;
		}
	}

	/* Reset endpoint before we start reading from it (mandatory) */
	verbose_reset_buffer(dev);
	
	struct timeval t1, t2;
	struct timeval timestamp;
	time_t current_time = time(NULL);
	struct tm *tm = localtime(&current_time);
	double diff, diff_file;

	if (sync_mode) {
		fprintf(stderr, "Reading samples in sync mode...\n");
		const char* recording_file_name = generate_iq_file_name(filename);
		file = create_iq_file(file, recording_file_name);

		while (!do_exit) {
	        gettimeofday(&t1, NULL);

			while(!do_exit) {
				
				FILE* new_file = NULL;
				gettimeofday(&t2, NULL);
				diff = (t2.tv_sec - t1.tv_sec) * 1000.0;
				diff += (t2.tv_usec - t1.tv_usec) / 1000.0;

				if (diff > ONE_SEC_TO_USEC*record_sec) { // check if it is necessary to create a new json file every second.
					current_time = time(NULL);
            		tm = localtime(&current_time);
								
					const char* recording_file_name = generate_iq_file_name(filename);
					fclose(file);
					file = create_iq_file(new_file, recording_file_name);
					gettimeofday(&t1, NULL);
					free((void *)recording_file_name);
				}

				r = rtlsdr_read_sync(dev, buffer, out_block_size, &n_read);
				if (r < 0) {
					fprintf(stderr, "WARNING: sync read failed.\n");
					break;
				}

				if ((bytes_to_read > 0) && (bytes_to_read < (uint32_t)n_read)) {
					n_read = bytes_to_read;
					do_exit = 1;
				}

				if (fwrite(buffer, 1, n_read, file) != (size_t)n_read) {
					fprintf(stderr, "Short write, samples lost, exiting!\n");
					break;
				}

				if ((uint32_t)n_read < out_block_size) {
					fprintf(stderr, "Short read, samples lost, exiting!\n");
					break;
				}

				if (bytes_to_read > 0)
					bytes_to_read -= n_read;
				}
		}
	} else {
		fprintf(stderr, "Reading samples in async mode...\n");
		r = rtlsdr_read_async(dev, rtlsdr_callback, (void *)file,
				      0, out_block_size);
	}

	if (do_exit)
		fprintf(stderr, "\nUser cancel, exiting...\n");
	else
		fprintf(stderr, "\nLibrary error %d, exiting...\n", r);

	if (file != stdout)
		fclose(file);

	rtlsdr_close(dev);
	free (buffer);
out:
	return r >= 0 ? r : -r;
}
